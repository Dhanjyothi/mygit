package com.example.virtusa.testservice;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.example.virtusa.model.Document;
import com.example.virtusa.service.IDocumentService;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes =TestBeanConfig.class)

public class JunitTest {
	@Autowired
	IDocumentService service;
	private Document document;
	@BeforeEach
	public void setUp()
	{
		document=new Document(1,"revathi",456,"ay56","tn220","chennai");
	}
	@Test
	public void saveDocument() {
		System.out.println("=========================================");
		Document doc=service.saveDocument(document);		
		assertEquals(doc.getName(),document.getName());
	}
	@Test
	public void getDocumentDetails()
	{
		service.getUserDocumentByName(document.getName()).forEach(System.out::println);
		
	}
	
}
