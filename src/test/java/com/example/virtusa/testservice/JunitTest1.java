package com.example.virtusa.testservice;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.example.virtusa.model.Document;
import com.example.virtusa.service.IDocumentService;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = TestBeanConfig.class)
public class JunitTest1 {
	@Autowired
	IDocumentService service;
	private Document document;
	@BeforeEach
	public void setUp()
	{
		document=new Document(5,"revathi",456,"ay56","tn220","chennai");
	}
	@Test
	public void saveDocument() {
		Document doc=service.saveDocument(document);
		System.out.println(doc);
		assertEquals(doc,document);
	}
	@Test
	public void getDocumentDetails()
	{
		List<Document> ls=service.getUserDocumentByName("revathi");
		assertEquals(ls.size(),2);
		
	}
	
}
