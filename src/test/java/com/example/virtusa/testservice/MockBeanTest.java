package com.example.virtusa.testservice;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.example.virtusa.model.Document;
import com.example.virtusa.repository.DocumentRepository;
import com.example.virtusa.service.impl.DocumentServiceImpl;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class MockBeanTest {

	@Autowired
	private DocumentServiceImpl service;
	
	@MockBean
	private DocumentRepository repo;
	
	private Document document;
	@BeforeEach
	public void setUp()
	{
		document=new Document(3,"revathi",456,"ay56","tn220","chennai");
	}
	@Test
	void saveDocument()
	{
		Mockito.when(repo.save(document)).thenReturn(document);
		Document doc=service.saveDocument(document);
		assertEquals(doc,document);
		
	}
}
