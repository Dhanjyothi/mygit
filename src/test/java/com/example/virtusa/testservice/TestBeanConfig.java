package com.example.virtusa.testservice;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan(basePackages="com")
public class TestBeanConfig {

}
