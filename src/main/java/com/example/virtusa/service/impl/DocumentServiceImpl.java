package com.example.virtusa.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.virtusa.model.Document;
import com.example.virtusa.repository.DocumentRepository;
import com.example.virtusa.service.IDocumentService;

import lombok.extern.slf4j.Slf4j;
@Service
@Transactional
@Slf4j
public class DocumentServiceImpl implements IDocumentService{
@Autowired
DocumentRepository repo;
	@Override
	public Document saveDocument(Document document) {
		System.out.println("Service Layer"+document);
		return repo.save(document);
	}
	@Override
	public List<Document> getUserDocumentByName(String name) {
		log.info(name);
	return repo.findByName(name);
	}
	
	@Override
	public List<Document> findAll() {
		return repo.findAll();
	}
	
}
