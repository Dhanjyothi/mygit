package com.example.virtusa.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.virtusa.model.Document;
import com.example.virtusa.service.IDocumentService;

@RestController
@RequestMapping("/")
//@Slf4j
public class DocumentController {
	@Autowired
	IDocumentService service;
	private Logger log=LoggerFactory.getLogger(DocumentController.class);
	@PostMapping("/user/document")
	public ResponseEntity<String> saveDocument(@RequestBody Document document)
	{
		try
		{
		log.info("Entered into saveDocument method");
		service.saveDocument(document);
		log.info("Document saved");
		return new ResponseEntity<>("Document saved",HttpStatus.CREATED);
		}
		catch(Exception e)
		{
			log.error("unable to save the document",e.getMessage());
			return new ResponseEntity<>("Unable to save document data",HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	@GetMapping("/user/document/{name}")
	public ResponseEntity<List<Document>> getByName(@PathVariable("name") String name)
	{
		try
		{
			log.info("Inside get method");
			List<Document> ls=service.getUserDocumentByName(name);
			log.info("Inside get method",ls);
			return new ResponseEntity<>(ls,HttpStatus.OK);
		}
		catch(Exception e)
		{
			System.out.println(e);
			//log.error("unable to save the document",e.getMessage());
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
}
