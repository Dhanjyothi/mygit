package com.example.virtusa.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Table(name="doc")
public class Document {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String name;
	private long aadhar;
	//@Column(unique = true)
	private String pan;
	private String address;
	private String license;
	public Document(int id,String name,long aadhae)
	{
		this.id=id;
		this.name=name;
		this.aadhar=aadhae;
	}
	

	@Override
	public String toString() {
		return "Doc [id=" + id + ", name=" + name + ", aadhar=" + aadhar + ", pan=" + pan + ", address=" + address
				+ ", license=" + license + "]";
	}
}
