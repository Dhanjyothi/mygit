package com.example.virtusa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.example.virtusa.model.AppProps;

@Component
public class MyRunner implements CommandLineRunner {
	@Autowired
	 AppProps p;
	@Override
	public void run(String... args) throws Exception {
		System.out.println(p.getName());
		System.out.println(p.getLang());
	}

}
